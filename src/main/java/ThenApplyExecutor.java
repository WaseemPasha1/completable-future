import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ThenApplyExecutor {
    public static void main(String args[]) throws ExecutionException, InterruptedException {
    //  Executor executor = Executors.newFixedThreadPool(2);
    CompletableFuture<String> future=    CompletableFuture.supplyAsync(() -> {
            return "Some result ";
        }).thenApplyAsync(res -> {
            // Executed in a thread obtained from the executor
            return res +"Processed Result ";

        }, Executors.newFixedThreadPool(1));

//        .thenApplyAsync(res1->{
//            return res1 + "third pool";
//        });
        System.out.println(future.get());
        System.out.println(Thread.currentThread().getName());
    }
}
