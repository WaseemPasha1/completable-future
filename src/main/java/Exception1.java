import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Exception1 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        String name="ameen";
      CompletableFuture<String> future=  CompletableFuture.supplyAsync(()-> {
            if (name == null) {
                throw new RuntimeException("computetional error");
            }
            return "hello ," +name;

        } ).handle((s,t)->s!=null?s:"hello strenger");
        System.out.println(future.get());
    }
}
