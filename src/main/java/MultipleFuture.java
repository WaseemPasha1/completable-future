import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class MultipleFuture {
    public static void main(String[] args) {
        //  static CompletableFuture<Void>	 allOf(CompletableFuture<?>... cfs)
            List<String> webPageLinks = Arrays.asList("","","");    // A list of 100 web page links

// Download contents of all the web pages asynchronously
        List<CompletableFuture<String>> pageContentFutures = webPageLinks.stream()
                .map(webPageLink -> downloadWebPage(webPageLink))
                .collect(Collectors.toList());


// Create a combined Future using allOf()
        CompletableFuture<Void> allFutures = CompletableFuture.allOf(
                pageContentFutures.toArray(new CompletableFuture[pageContentFutures.size()])
        );
    }

    private static CompletableFuture<String> downloadWebPage(String pageLink){
        return CompletableFuture.supplyAsync(() -> {
            // Code to download and return the web page's content
            return pageLink;
        });
    }

}