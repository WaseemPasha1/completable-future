import java.util.concurrent.CompletableFuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

public class RunAsyn {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture.runAsync(() -> {
            System.out.println("my task1 is completed");
        });
       System.out.println("------------------------");
        CompletableFuture.runAsync(() -> {
            System.out.println("my task2 is completed");
        }, Executors.newFixedThreadPool(2));
        System.out.println("--------------------------------");

        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> "hello");
        System.out.println(future.get());
        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            return "hello world";
        },Executors.newFixedThreadPool(2));
        System.out.println(future2.get());

    }
}