import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

public class DisplayList{

    public static void main(String[] args) {
       CompletableFuture<String> completableFuture=calculateAsynce();
        try {
           String result= completableFuture.get();
            System.out.println(result);
            System.out.println(Thread.currentThread().getName());
            System.out.println("---------------------------------");
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();

        }
    }
        private static CompletableFuture<String>  calculateAsynce() {
        CompletableFuture<String> completableFuture=new CompletableFuture<>();
        List<String> list = new ArrayList<>();
        list.add("kk");
        list.add("kr");
        list.add("az");
        Executors.newCachedThreadPool().submit(()->{
            try {
                Thread.sleep(1);
              String joinStr=  String.join(",",list);
                System.out.println(Thread.currentThread().getName());
              completableFuture.complete(joinStr);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        });
        return completableFuture;
    }
}
