import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Exception2 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        String name=null;
        CompletableFuture<String> future=  CompletableFuture.supplyAsync(()-> {
            if (name == null) {
                throw new RuntimeException("computetional error");
            }
            return "hello ," +name;

        } );
        System.out.println(future.get());

    }
}
