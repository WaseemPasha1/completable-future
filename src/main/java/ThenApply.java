import java.util.concurrent.*;

public class ThenApply {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

      CompletableFuture<String> future=  CompletableFuture.supplyAsync(() -> {
//            try {
//                TimeUnit.SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                throw new IllegalStateException(e);
//            }
            return "Some Result";
        }).thenApplyAsync(result -> {
    /*
      Executed in the same thread where the supplyAsync() task is executed
      or in the main thread If the supplyAsync() task completes immediately (Remove sleep() call to verify)
    */
            return result+ "Processed Result";
        });
        System.out.println(future.get());
    }
}
