import java.util.Set;
import java.util.TreeSet;

public class Tree {
    public static void main(String[] args) {
        Set<Integer> treeSet = new TreeSet<>();

        treeSet.add(3);
        treeSet.add(1);
        treeSet.add(3);
        treeSet.add(2);

        for (int i : treeSet) {
            System.out.println(i);
        }
    }

}
